(* A self-contained microbenchmark of Buffer functionxss *)

(* This modules reuses stdlib code,
   and adds a few variant of Buffer.add_char and Buffer.add_string *)
module Buffer = struct

type t =
 {mutable buffer : bytes;
  mutable position : int;
  mutable length : int;
  initial_buffer : bytes}

(* exactly the standard code *)
let create n =
 let n = if n < 1 then 1 else n in
 let n = if n > Sys.max_string_length then Sys.max_string_length else n in
 let s = Bytes.create n in
 {buffer = s; position = 0; length = n; initial_buffer = s}

(* exactly the standard code *)
let reset b =
  b.position <- 0;
  b.buffer <- b.initial_buffer;
  b.length <- Bytes.length b.buffer

(* exactly the standard code *)
let resize b more =
  let old_pos = b.position in
  let old_len = b.length in
  let new_len = ref old_len in
  while old_pos + more > !new_len do new_len := 2 * !new_len done;
  assert (!new_len > 0);
  if !new_len > Sys.max_string_length then begin
    if old_pos + more <= Sys.max_string_length
    then new_len := Sys.max_string_length
    else failwith "Buffer.add: cannot grow buffer"
  end;
  let new_buffer = Bytes.create !new_len in
  Bytes.blit b.buffer 0 new_buffer 0 b.position;
  assert (b.position <= b.length);
  assert (b.position <= !new_len);
  b.buffer <- new_buffer;
  b.length <- !new_len;
  assert (b.position + more <= b.length);
  assert (old_pos + more <= b.length);
  assert (Bytes.length b.buffer >= b.length);
  ()

(* exactly the standard code *)
let add_char_std b c =
  let pos = b.position in
  if pos >= b.length then resize b 1;
  Bytes.unsafe_set b.buffer pos c;
  b.position <- pos + 1

(* the standard code with a weird transformation
   to avoid spilling *)
let add_char_std_nospill b c =
  let pos = b.position in
  if pos >= b.length then begin
    resize b 1;
    Bytes.unsafe_set b.buffer pos c;
  end else
    Bytes.unsafe_set b.buffer pos c;
  b.position <- pos + 1

(* a safe version with Bytes.set instead of Bytes.unsafe_set *)
let add_char_safe b c =
  let pos = b.position in
  if pos >= b.length then resize b 1;
  Bytes.set b.buffer pos c;
  b.position <- pos + 1

(* safe version, tuned to avoid spilling *)
let add_char_safe_nospill b c =
  let pos = b.position in
  if pos >= b.length then
    (resize b 1;
     Bytes.set b.buffer pos c) 
  else
    (Bytes.set b.buffer pos c);
  b.position <- pos + 1

(* not-really-safe shortcut, assumes b.length and b.buffer are in sync *)
let add_char_test_wrong b c =
  let {position; buffer; length} = b in
  if position < length then
    Bytes.unsafe_set buffer position c
  else begin
    resize b 1;
    Bytes.set b.buffer b.position c
  end;
  b.position <- b.position + 1

(* safe shortcut, does not Bytes.length call *)
let add_char_test b c =
  let {position; buffer} = b in
  if position < Bytes.length buffer then
    Bytes.unsafe_set buffer position c
  else begin
    resize b 1;
    Bytes.set b.buffer b.position c
  end;
  b.position <- b.position + 1

(* safe shortcut without a Bytes.set case at all *)
let rec add_char_rec b c =
  let {position; buffer} = b in
  if position >= Bytes.length buffer then begin
    resize b 1;
    add_char_rec b c
  end else begin
    Bytes.unsafe_set buffer position c;
    b.position <- b.position + 1
  end

(* standard add_string *)
let add_string_std b s =
  let len = String.length s in
  let new_position = b.position + len in
  if new_position > b.length then resize b len;
  Bytes.unsafe_blit_string s 0 b.buffer b.position len;
  b.position <- new_position

(* add_string, with the spilling tuning *)
let add_string_std_nospill b s =
  let len = String.length s in
  let new_position = b.position + len in
  if new_position > b.length then
    (resize b len;
     Bytes.unsafe_blit_string s 0 b.buffer b.position len)
  else Bytes.unsafe_blit_string s 0 b.buffer b.position len;
  b.position <- new_position
end

module Buffer2 = struct
  (* A modified buffer module where the bytes buffer and its length
     are packed together in an *immutable* block. This adds
     indirection but it gives the guarantee (I think?) that concurrent
     updates will keep the buffer and its length synchronized,
     allowing to use the cached .length field instead of recomputing
     Bytes.length. *)
type data = { buffer: bytes; length: int }
type t =
 {mutable data : data;
  mutable position : int;
  initial_data : data}

let create n =
 let n = if n < 1 then 1 else n in
 let n = if n > Sys.max_string_length then Sys.max_string_length else n in
 let buffer = Bytes.create n in
 let data = { buffer; length = n } in
 {data; position = 0; initial_data = data}

let reset b =
  b.position <- 0;
  b.data <- b.initial_data

let resize b more =
  let old_pos = b.position in
  let old_len = b.data.length in
  let new_len = ref old_len in
  while old_pos + more > !new_len do new_len := 2 * !new_len done;
  assert (!new_len > 0);
  if !new_len > Sys.max_string_length then begin
    if old_pos + more <= Sys.max_string_length
    then new_len := Sys.max_string_length
    else failwith "Buffer.add: cannot grow buffer"
  end;
  let new_buffer = Bytes.create !new_len in
  Bytes.blit b.data.buffer 0 new_buffer 0 b.position;
  assert (b.position <= b.data.length);
  assert (b.position <= !new_len);
  let new_data = {buffer = new_buffer; length = !new_len} in
  b.data <- new_data;
  assert (b.position + more <= b.data.length);
  assert (old_pos + more <= b.data.length);
  assert (Bytes.length b.data.buffer >= b.data.length);
  ()

let add_char_data_std b c =
  let pos = b.position in
  if pos >= b.data.length then resize b 1;
  Bytes.unsafe_set b.data.buffer pos c;
  b.position <- pos + 1

let add_char_data_std_nospill b c =
  let pos = b.position in
  if pos >= b.data.length then
    (resize b 1;
     Bytes.unsafe_set b.data.buffer pos c)
  else Bytes.unsafe_set b.data.buffer pos c;
  b.position <- pos + 1

(* safe version without the Bytes.length check *)
let add_char_data_safe b c =
  let pos = b.position in
  let {buffer; length} = b.data in
  if pos < length then
    Bytes.unsafe_set buffer pos c
  else 
    (resize b 1;
     Bytes.set b.data.buffer b.position c);
  b.position <- pos + 1
end

(* Boring benchmark code *)
let benchmark_std n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_std buf 'a';
    done;
  done

let benchmark_std_nospill n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_std_nospill buf 'a';
    done;
  done

let benchmark_safe n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_safe buf 'a';
    done;
  done

let benchmark_safe_nospill n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_safe_nospill buf 'a';
    done;
  done

let benchmark_test n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_test buf 'a';
    done;
  done

let benchmark_test_wrong n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_test_wrong buf 'a';
    done;
  done

let benchmark_rec n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_rec buf 'a';
    done;
  done

(* string benchmarks *)
let benchmark_string_std n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_string_std buf "aaaa";
    done;
  done

let benchmark_string_std_nospill n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_string_std_nospill buf "aaaa";
    done;
  done

let benchmark_std n =
  let buf = Buffer.create 1 in
  for _ = 1 to n do
    Buffer.reset buf;
    for _ = 1 to 1050 do
      Buffer.add_char_std buf 'a';
    done;
  done

(* 'data' benchmarks *)
let benchmark_data_std n =
  let buf = Buffer2.create 1 in
  for _ = 1 to n do
    Buffer2.reset buf;
    for _ = 1 to 1050 do
      Buffer2.add_char_data_std buf 'a';
    done;
  done

let benchmark_data_std_nospill n =
  let buf = Buffer2.create 1 in
  for _ = 1 to n do
    Buffer2.reset buf;
    for _ = 1 to 1050 do
      Buffer2.add_char_data_std_nospill buf 'a';
    done;
  done

let benchmark_data_safe n =
  let buf = Buffer2.create 1 in
  for _ = 1 to n do
    Buffer2.reset buf;
    for _ = 1 to 1050 do
      Buffer2.add_char_data_safe buf 'a';
    done;
  done


let impls = [
  (* standard code, the latter tuned manually to avoid spilling *)
  "std", benchmark_std;
  "std_nospill", benchmark_std_nospill;

  (* replacing Bytes.unsafe_set with Bytes.set *)
  "safe", benchmark_safe;
  "safe_nospill", benchmark_safe_nospill;

  (* these three versions shortcut to Bytes.unsafe_set when no
     resizing was necessary -- see their definitions for details. *)
  "test", benchmark_test;
  "test_wrong", benchmark_test_wrong;
  "rec", benchmark_rec;

  (* these versions use a slightly different data representation *)
  "data_std", benchmark_data_std;
  "data_std_nospill", benchmark_data_std_nospill;
  "data_safe", benchmark_data_safe;

  (* test influence of spilling on add_string *)
  "string_std", benchmark_string_std;
  "string_std_nospill", benchmark_string_std_nospill;
]

let () =
  Printf.eprintf "usage: %s <niter> [%s]\n%!"
    Sys.argv.(0)
    (String.concat " | " (List.map fst impls));
  let niter = int_of_string Sys.argv.(1) in
  let impl = List.assoc Sys.argv.(2) impls in
  ignore (impl niter)
